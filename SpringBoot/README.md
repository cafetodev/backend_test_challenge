# cdk_spring_seed
Api Version: 0.1.0

This is the repository for all backend stuff related to the Spring seed project.

- Spring Boot framework 2.3
- Documentation support for API endpoints by Swagger
- Static code analysis with checkstyle
- Unit testing support with Junit and spring boot test

# Start Up
- Pull the project to your local machine.
- Run the server executing `gradlew bootRun`

# Testing Commands
- Analysis with checkstyle `gradlew checkstyleMain checkstyleTest`
- Run test suit `gradlew test`
