/*
 * This source code is the confidential, proprietary information of
 * Cafeto Software S.A.S., you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with Cafeto Software S.A.S.
 *
 * 2019: Cafeto Software S.A.S.
 * All Rights Reserved.
 */

package co.cafeto.cdkspringseed;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.TestPropertySource;

/**
 * Test class for {@link ApplicationStarter}.
 *
 * @author Daniel Mejia
 */
@SpringBootTest
@TestPropertySource("classpath:application.properties")
public class ApplicationStarterTest
{
  @Mock
  SpringApplication springApplication;

  @Mock
  ConfigurableApplicationContext configurableApplicationContext;

  @Before
  public void setUp()
  {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testMain()
  {
    ApplicationStarter.main(new String[] {});
  }
}