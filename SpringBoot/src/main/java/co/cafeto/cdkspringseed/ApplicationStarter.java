/*
 * This source code is the confidential, proprietary information of
 * Cafeto Software S.A.S., you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with Cafeto Software S.A.S.
 *
 * 2019: Cafeto Software S.A.S.
 * All Rights Reserved.
 */

package co.cafeto.cdkspringseed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * Contains main method for run Spring Boot Application.
 *
 * @author Daniel Mejia
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class ApplicationStarter
{
    /**
     * Main application run.
     *
     * @param args arguments for main application
     */
    public static void main(String[] args)
    {
        SpringApplication.run(ApplicationStarter.class, args);
    }
}
