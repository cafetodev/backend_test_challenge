/*
 * This source code is the confidential, proprietary information of
 * Cafeto Software S.A.S., you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with Cafeto Software S.A.S.
 *
 * 2019: Cafeto Software S.A.S.
 * All Rights Reserved.
 */

package co.cafeto.cdkspringseed.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configuration for swagger API documentation.
 *
 * @author Edson Ruiz Ramirez
 * @author Nelson Diaz
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration extends WebMvcConfigurationSupport
{
    @Value("${swagger.title}")
    private String swaggerTitle;

    @Value("${swagger.description}")
    private String swaggerDescription;

    @Value("${swagger.enabled}")
    private boolean swaggerEnabled;

    /**
     * Initialize Swagger UI API Documentation.
     *
     * @return Docket for Swagger initialize.
     */
    @Bean
    public Docket swaggerInit()
    {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
            .apiInfo(swaggerInfo())
            .enable(swaggerEnabled);
    }

    /**
     * apiInfo builder method for Swagger.
     *
     * @return ApiInfo with Title and Description for Swagger UI site.
     */
    private ApiInfo swaggerInfo()
    {
        return new ApiInfoBuilder()
            .title(swaggerTitle)
            .description(swaggerDescription)
            .build();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations(
            "classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**").addResourceLocations(
            "classpath:/META-INF/resources/webjars/");
    }
}