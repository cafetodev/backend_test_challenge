# Seed to Node JS Typescript projects with NestJS and CRUD default implementation.

## Getting started

Before start project is neccesary install the following dependencies in your system:

* [Git](https://git-scm.com/)
* [NodeJs Version 10.14.1](https://nodejs.org)

## Installation
```bash
# Clone the repository
$ git clone git@bitbucket.org:cafetodev/cafeto.seed.node.nestjs.git

# Install a database driver
npm install pg | mysql | sqlite3 | mssql | oracledb | mongodb  --save

# Install the NestJs CLI
$ npm i -g @nestjs/cli

# Install the dependencies
$ npm run install
```

## Configuration 
Create the `.env` file, to run backend locally:

```.env
# Environment properties
NODE_ENV = 'development'
SERVER_PORT = 3000

# Database properties
TYPEORM_CONNECTION = ''
TYPEORM_HOST = ''
TYPEORM_USERNAME = ''
TYPEORM_PASSWORD = ''
TYPEORM_DATABASE = ''
TYPEORM_PORT = ''
TYPEORM_ENTITIES = src/**/*.entity.ts
TYPEORM_MIGRATIONS = src/modules/database/migrations/*.ts
TYPEORM_MIGRATIONS_DIR = src/modules/database/migrations
```

## Migrations

### 

```bash
# Generate the migration to update the current model with database
npm run typeorm migration:generate -n 'migrationName'

# Create an empty migration file
npm run typeorm migration:create -n 'migrationName'

# Run the migration
npm run typeorm migration:run

# Rolback the migration
npm run typeorm migration:revert
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Quality Unit Test and TsLint

```bash
# unit tests
$ npm run test.

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov

# TsLint analysis
$ npm run lint
```

## More information
- NestJs framework - [Nest Js](https://docs.nestjs.com/)
- NestJsx CRUD operations - [NestJsx CRUD](https://github.com/nestjsx/crud)
- API documentation - [NestJs Swagger](https://docs.nestjs.com/recipes/swagger)
- Database ORM system - [TypeOrm](https://github.com/typeorm/typeorm)
- Unit testing - [Jest](https://jestjs.io/)

## Stay in touch

- Website - [http://cafeto.co/](http://cafeto.co/)
