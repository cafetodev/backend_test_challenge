/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppConfig } from './config/app.config';
import { AppModule } from './modules/app.module';

/**
 * Main file to start the application.
 *
 * @author Daniel Mejia
 * @file main.ts
 */
async function bootstrap() {
  const app: INestApplication = await NestFactory.create(AppModule);
  AppConfig.configure(app);
  await app.listen(process.env.SERVER_PORT);
}
bootstrap();
