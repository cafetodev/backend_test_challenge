/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Entity, EntityOptions } from 'typeorm';

/**
 * Extendend entity decorator to disabled on test environment.
 *
 * @param { EntityOptions } options The Entity decorator options.
 */
export const EntityExtended = (options?: EntityOptions) => process.env.NODE_ENV === 'test' ? () => { } : Entity(options);
