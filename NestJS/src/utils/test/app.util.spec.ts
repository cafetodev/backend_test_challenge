/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { EntityExtended } from '../app.util';

/**
 * Utils test file for {@link app.util.ts}
 *
 * @author Daniel Mejia
 * @File app.util.spec.ts
 */
describe('Utils: Test suit', () => {

  describe('EntityExtended', () => {
    const environment = process.env.NODE_ENV;

    it('should get decorator in test mode', () => {
      const decorator = EntityExtended();
      expect(decorator()).toBeUndefined();
    });

    it('should get decorator in another mode', () => {
      process.env.NODE_ENV = 'another';
      const decorator = EntityExtended();
      expect(decorator).toBeDefined();
    });

    afterEach(() => {
      process.env.NODE_ENV = environment;
    });
  });
});
