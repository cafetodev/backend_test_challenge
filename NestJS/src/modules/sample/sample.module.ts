/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SampleController } from './sample.controller';

// Services
import { SampleService } from './sample.service';

// Entities
import { SampleEntity } from './sample.entity';

/**
 * The Sample Module
 *
 * @author Daniel Mejia
 * @Class SampleModule
 */
@Module({
  imports: [TypeOrmModule.forFeature([SampleEntity])],
  controllers: [SampleController],
  providers: [SampleService],
})
export class SampleModule {}
