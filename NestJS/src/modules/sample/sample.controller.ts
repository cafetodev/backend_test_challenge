/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';

// Services
import { SampleService } from './sample.service';

// Entities
import { SampleEntity } from './sample.entity';

/**
 * Controller for {@link SampleEntity}
 *
 * @author Daniel Mejia
 * @Class SampleController
 */
@Crud({
  model: {
    type: SampleEntity
  }
})
@Controller(SampleEntity.SAMPLES_RESOURCE)
export class SampleController implements CrudController<SampleEntity> {

  /**
   * Constructor.
   *
   * @param { SampleService } sampleService The test service to manipulate Test entity.
   */
  constructor(public readonly service: SampleService) { }
}
