/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Test, TestingModule } from '@nestjs/testing';
import { SampleController } from '../sample.controller';

// Services
import { SampleService } from '../sample.service';

/**
 * Controller test file for {@link SampleController}
 *
 * @author Daniel Mejia
 * @File sample.controller.spec.ts
 */
describe('SampleController: Test suit', () => {
  let sampleController: SampleController;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SampleController],
      providers: [
        {
          provide: SampleService,
          useValue: {},
        },
      ],
    }).compile();
    sampleController = module.get<SampleController>(SampleController);
  });

  it('should get instance fine', () => {
    expect(sampleController).toBeTruthy();
  });
});
