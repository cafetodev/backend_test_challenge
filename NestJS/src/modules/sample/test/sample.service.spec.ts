/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Test, TestingModule } from '@nestjs/testing';
import { mockRepositoryProvider } from '../../generic/test/mocks';

// Services
import { SampleService } from '../sample.service';

// Entities
import { SampleEntity } from '../sample.entity';

/**
 * Service test file for {@link SampleService}
 *
 * @author Daniel Mejia
 * @File sample.service.spec.ts
 */
describe('SampleService: Test suit', () => {
  let sampleService: SampleService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SampleService,
        mockRepositoryProvider<SampleEntity>(SampleEntity),
      ],
    }).compile();

    sampleService = module.get<SampleService>(SampleService);
  });

  it('should get instance fine', () => {
    expect(sampleService).toBeDefined();
  });
});
