/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

import { IsNotEmpty, IsString } from 'class-validator';
import { Column } from 'typeorm';
import { EntityExtended } from '../../utils/app.util';
import { GenericEntity } from '../generic/generic.entity';

/**
 * Sample entity for Sample table.
 *
 * @author Daniel Mejia
 * @Class SampleEntity
 */
@EntityExtended()
export class SampleEntity extends GenericEntity {

  /**
   * Samples resource.
   *
   * @type { String }
   */
  public static SAMPLES_RESOURCE = 'samples';

  /**
   * The name for the sample.
   *
   * @type { string }
   */
  @Column()
  @IsString({ always: true })
  @IsNotEmpty()
  name: string;
}
