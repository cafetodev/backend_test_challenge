/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

// Services
import { GenericService } from '../generic/generic.service';

// Entities
import { SampleEntity } from './sample.entity';

/**
 * Service for {@link SampleEntity}
 *
 * @author Daniel Mejia
 * @Class SampleService
 */
@Injectable()
export class SampleService extends GenericService<SampleEntity> {

  /**
   * Constructor.
   *
   * @param { Repository<SampleEntity> } sampleRepository The sample repository to manipulate the entity.
   */
  constructor(@InjectRepository(SampleEntity) sampleRepository: Repository<SampleEntity>) {
    super(sampleRepository);
  }
}
