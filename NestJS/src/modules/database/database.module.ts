/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';

/**
 * Database module for configuration.
 *
 * @author Daniel Mejia
 * @Class DatabaseModule
 */
@Module({
  imports: [TypeOrmModule.forRoot()],
})
export class DatabaseModule {

  /**
   * Constructor for database module initialization.
   *
   * @param { Connection } connection The database connection.
   */
  constructor(private readonly connection: Connection) { }
}
