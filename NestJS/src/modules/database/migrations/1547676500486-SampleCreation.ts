/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { MigrationInterface, QueryRunner, Table } from 'typeorm';

/**
 * @author Daniel Mejia
 * @Class SampleCreation1547676500486
 * @description Sample migration to create the sample table in database.
 */
export class SampleCreation1547676500486 implements MigrationInterface {

  /**
   * Executes the creation table when we run the migration.
   *
   * @param { QueryRunner } queryRunner The migration runner.
   */
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'sample_entity',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar',
          },
          {
            name: 'createdAt',
            type: 'TIMESTAMP',
            default: 'now()',
          },
          {
            name: 'updatedAt',
            type: 'TIMESTAMP',
            default: 'now()',
          },
        ],
      }),
      true,
    );
  }

  /**
   * Executes the drop table when we revert the migration.
   *
   * @param { QueryRunner } queryRunner The migration runner.
   */
  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('sample_entity');
  }
}
