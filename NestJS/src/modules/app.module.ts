/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { SampleModule } from './sample/sample.module';

/**
 * App Module.
 *
 * @author Daniel Mejia
 * @Class AppModule
 */
@Module({
  imports: [DatabaseModule, SampleModule],
})
export class AppModule {}
