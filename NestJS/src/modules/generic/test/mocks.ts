/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Type, ValueProvider } from '@nestjs/common/interfaces';
import { getRepositoryToken } from '@nestjs/typeorm';
import { GenericEntity } from '../generic.entity';
import { Repository } from 'typeorm';

/**
 * Generates a mock repository provider for a given entity.
 *
 * @template T The entity type.
 * @param { Type<T> } entity The entity Type for generate the repository.
 * @returns { ValueProvider } The provider for the given entity.
 */
export function mockRepositoryProvider<T extends GenericEntity>(entity: Type<T>): ValueProvider {
  const mkFailFn = (name: string) => () => {
    throw new Error(`unexpected/unmocked call to Repository<${entity.name}>.${name}`);
  };

  const mockProperties = {
    metadata: { columns: [], relations: [] },
  };

  const mockFunctions: Partial<Repository<T>> = [
    'find',
    'findOne',
    'findOneOrFail',
    'save',
    'delete',
    'remove',
  ].reduce(
    (acc, fnName) => {
      acc[fnName] = mkFailFn(fnName);
      return acc;
    },
    {},
  );

  return {
    provide: getRepositoryToken(entity),
    useValue: { ...mockProperties, ...mockFunctions },
  };
}
