/**
 * PLEASE ADD HERE HEADER COPYRIGHT...
 */

// Dependencies
import { Repository } from 'typeorm';
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";

// Entities
import { GenericEntity } from './generic.entity';

/**
 * Generic service with default actions for {@link TypeOrmCrudService}
 *
 * @author Daniel Mejia
 * @Class GenericService
 */
export class GenericService<T extends GenericEntity> extends TypeOrmCrudService<T> {

  /**
   * Constructor for generic service.
   *
   * @param { Repository<T> } repository The entity repository.
   */
  constructor(repository: Repository<T>) {
    super(repository);
  }
}
